extends CharacterBody2D

@export var SPEED = 300.0
@export var JUMP_VELOCITY = -400.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
@onready var player_sprite : AnimatedSprite2D = $AnimatedSprite2D
@onready var player_body : CharacterBody2D = self


func _physics_process(delta):
	# Add the gravity.
	if not player_body.is_on_floor():
		player_body.velocity.y += gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("ui_accept") and player_body.is_on_floor():
		player_body.velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("ui_left", "ui_right")
	if direction:
		player_body.velocity.x = direction * SPEED
	else:
		player_body.velocity.x = move_toward(player_body.velocity.x, 0, SPEED)
	
	if player_body.velocity.x > 0:
		player_sprite.flip_h = false
	if player_body.velocity.x < 0:
		player_sprite.flip_h = true

	player_body.move_and_slide()
#	print($AnimatedSprite2D.animation)
	if abs(player_body.velocity.x) > 1 and player_sprite.animation != "running":
		player_sprite.play("running")
	if abs(player_body.velocity.x) <= 1 and player_sprite.animation == "running":
		player_sprite.play("idle")
