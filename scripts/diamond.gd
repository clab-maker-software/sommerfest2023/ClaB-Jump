extends Node2D

var lifetime : float = 0.0
var original_position : Vector2 = Vector2.ZERO
# Called when the node enters the scene tree for the first time.
func _ready():
	original_position = position
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	lifetime += delta
	position.y = original_position.y + sin(lifetime)* 5
	pass


func _on_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	GameLogic.add_coin.emit()
	queue_free()
	pass # Replace with function body.
