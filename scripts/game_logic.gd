extends Node


signal open_door
signal reset_player
signal update_ui
signal add_key
signal add_coin
signal win_level
signal spike_hit

var level_player_spawn_position : Vector2 = Vector2.ZERO
var level_keys_total = 0
var level_keys_gathered = 0
var coins_gathered = 0
var current_level_id = 0
var current_level : Node2D = null


var player : CharacterBody2D = null

var levels : Array[String] = ["level.tscn","level2.tscn","you_win.tscn"]

func _ready():
	add_key.connect(_on_add_key)
	add_coin.connect(_on_add_coin)
	win_level.connect(_on_win_level)
	spike_hit.connect(_on_spike_hit)
	
func _on_win_level():
	current_level_id += 1
	if current_level_id < len(levels):
		print("load next level")
		var level : Node2D= load(levels[current_level_id]).instantiate()
		current_level.queue_free()
		get_tree().get_root().call_deferred("add_child",level)
		
func _on_add_key():
	level_keys_gathered += 1
	if level_keys_gathered >= level_keys_total:
		open_door.emit()
	update_ui.emit()
	
func _on_add_coin():
	coins_gathered += 1
	update_ui.emit()

func _on_spike_hit():
	player.position = level_player_spawn_position
	player.velocity = Vector2.ZERO
